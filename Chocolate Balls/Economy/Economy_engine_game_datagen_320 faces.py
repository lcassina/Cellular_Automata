# -*- coding: utf-8 -*-
"""
Created 01 July 2018

@author: Hellmer Luis Rahms
"""

import pandas as pd
import matplotlib.pyplot as plt
#from matplotlib import style
import numpy as np
style.use ("ggplot")
import math
import random
import time

############################### INPUTS // DATASETS ###########################################

economy=pd.read_csv("T320_Economy_data.csv") #csv with economic data per triangle
collist=list(economy.columns)


economy["GDPcap"]=economy["GDP"]/economy["Population"]*1000000 #create GDP per capita
economy.fillna(0, inplace=True)

pro = pd.read_csv("Adjacencies_320.csv") #dataframe with adjacencies
prox = np.array(pro)

econl = economy [[ "Trade", "Unemp", "Resources", "IO", "EPI"]]


landuse=pd.read_csv("Land use.csv") #csv with land use data
landuse.fillna(0, inplace=True)

rng = range(0,320) #cells to simulate
migration = 1000

############################### GENERAL DATASET OPERATORS ###########################################


def col_average (datafrm, col_name): #creates average from a column
    col_value=datafrm[[col_name]]
    nums=col_value.loc[1:len(col_value)]
    y=np.mean(nums)
    x=float(y)
    return x

def mean(numbers):
    return float(sum(numbers)) / max(len(numbers), 1)

def df_discretiser_10(df, dfcomp): # discretises dataframe into highs and lows depending on their average
    df2=pd.DataFrame() 
    def col_average (datafrm, col_name): #creates average from a column
        col_value=datafrm[[col_name]]
        nums=col_value.loc[1:len(col_value)]
        y=np.mean(nums)
        x=float(y)
        return x  
    collist=list(df.columns)
    for i in collist:
        bins = [dfcomp[i].min(),0.1*dfcomp[i].max(),0.2*dfcomp[i].max(),0.3*dfcomp[i].max(),0.4*dfcomp[i].max(),0.5*dfcomp[i].max(),0.6*dfcomp[i].max(),0.7*dfcomp[i].max(),0.8*dfcomp[i].max(),0.9*dfcomp[i].max(),dfcomp[i].max()]
        names = [0,1,2,3,4,5,6,7,8,9]
        df2[i]=pd.cut(df[i], bins, labels = names)
    df2.fillna(0, inplace=True)
    return df2

def logistic (func_range, midpoint, steepness, domain): #function for smooth transition between numbers
    return func_range / (1+np.exp(-1 * steepness * (domain-midpoint)))



############################### SCENARIO COMPUTATION ###########################################

def sim_iso(df, tradebarriers): #returns a dataframe that represents the country on isolationsim after five years
    economy_tx=pd.DataFrame()
    # if t < 5
    if tradebarriers==False:
        return df
    else: #equations/mathematical model
        economy_tx["Unemp"]=df["Unemp"]+df["Trade"]*0.1
        economy_tx["EPI"]=0.05*df["Trade"]+df["EPI"]
        economy_tx["IO"]=(df["Resources"]/6)*df["IO"]
        economy_tx["Trade"]=df["Trade"]*0.01
        economy_tx["Resources"]=abs(df["Resources"]-(df["Resources"]*(df["Resources"]*(1/(economy_tx["IO"]-df["IO"])))))
        economy_tx["GDP"]=df["GDP"]*((((economy_tx["Resources"]/df["Resources"])+(economy_tx["IO"]/df["IO"])+(economy_tx["Trade"]/df["Trade"])/3)*0.8))
        economy_tx["GDPcap"] = economy_tx["GDP"]/df["Population"]*1000000
        economy_tx["Score"] = economy_tx["GDPcap"]
    economy_tx = economy_tx[["GDP", "GDPcap", "Trade", "Unemp", "Resources", "IO", "EPI","Score"]]
    economy_tx.fillna(0, inplace=True)   
    return economy_tx

def sim_ai (df, ai): # Artificial Intelligence
    economy_ai=pd.DataFrame()
    if ai == False:
        return df
    if ai == True: #equations/mathematical model
        economy_ai["Unemp"]= df["Unemp"]*(45/df["Unemp"])
        economy_ai["IO"]=df["IO"]*(1+((economy_ai["Unemp"]-df["Unemp"])/100))
        economy_ai["Trade"]=df["Trade"]*(1+((economy_ai["IO"]-df["IO"])/100))
        economy_ai["Resources"]=df["Resources"]*(1+((economy_ai["IO"]-df["IO"])/100)+((economy_ai["Trade"]-df["Trade"])/100))
        economy_ai["EPI"]= df["EPI"]+df["EPI"]*(((economy_ai["IO"]-df["IO"])/100)+((economy_ai["Resources"]-df["Resources"])/100) + ((economy_ai["Trade"]-df["Trade"])/100)/3)
        economy_ai["GDP"]=df["GDP"]*((((economy_ai["Resources"]/df["Resources"])+(economy_ai["IO"]/df["IO"])+(economy_ai["Trade"]/df["Trade"])/3)*0.8))
        economy_ai["GDPcap"] = economy_ai["GDP"]/df["Population"]*1000000
        economy_ai["Score"] = economy_ai["GDPcap"]
    economy_ai = economy_ai[["GDP", "GDPcap", "Trade", "Unemp", "Resources", "IO", "EPI","Score"]]
    economy_ai.fillna(0, inplace=True)
    return economy_ai  


############################### SIMULATION DATAFRAME GENERATION ###########################################
    
#we used houdini to visualize the data. The ideal format for houdini to read is a csv for each
#attribute that contains the triangles as rows and years as columns.

#Global names    
isolation = "isolation"
ai = "ai"
    
def sim_std (df, factor): #business as usual with cosine function for liveliness
    def cos_fn (rng,line):
        x = rng
        a = abs(np.cos(x)*1.5+line)
        return a
    domain1 = frange(0,9,1)   
    array  = []
    for i in range (0,320): # amount of triangles
        df0 = list(df[factor])# triangles
        a = (cos_fn(domain1,df0[i]))
        array.append(a)  
    df_new = pd.DataFrame(np.array(array))
    df_new.fillna(0, inplace=True)
    return df_new
    
def sim_iso_fl (df,factor): #simulates trade barriers with fluid transition, returns TEN years/rows
    sim = frange(0,9,1)   
    ec = sim_iso(df, tradebarriers = True)
    df0 = list(df[factor])
    dfx = list(ec[factor])
    array  = []
    for i in range (0,320): # triangles
        a = list(logistic(dfx[i]-df0[i],4,1,sim) + df0[i])
        array.append(a)
    df_new = pd.DataFrame(np.array(array))
    df_new.fillna(0, inplace=True)
    return df_new

def sim_ai_fl (df,factor): #simulates artificial intelligence with fluid transition, returns TEN years/rows
    sim = frange(0,9,1)   
    ec = sim_ai(df, ai = True)
    df0 = list(df[factor])
    dfx = list(ec[factor])
    array  = []
    for i in range (0,320): # triangles
        a = list(logistic(dfx[i]-df0[i],4,1,sim) + df0[i])
        array.append(a)
    df_new = pd.DataFrame(np.array(array))
    df_new.fillna(0, inplace=True)
    return df_new

def investment_hd (df, factor,length): #simulates investment in human development
    dfx = pd.DataFrame()    
    if factor == "GDP":
        dfx[0] = df[factor]
        x = 1
        while x < length:
            dfx[x] = dfx[x-1]*1.01
            x += 1
        return dfx
    if factor == "Trade":
        dfx[0] = df[factor]
        x = 1
        while x < length: 
            dfx[x] = dfx[x-1]*1
            x += 1
        return dfx
    if factor == "Unemp":
        dfx[0] = df[factor]
        x = 1
        while x < length:
            dfx[x] = dfx[x-1]*0.99
            x += 1
        return dfx
    if factor == "Resources":
        dfx[0] = df[factor]
        x = 1
        while x < length:
            dfx[x] = dfx[x-1]*0.99
            x += 1
        return dfx
    if factor == "IO":
        dfx[0] = df[factor]
        x = 1
        while x < length:
            dfx[x] = dfx[x-1]*0.99
            x += 1
        return dfx
    if factor == "EPI":
        dfx[0] = df[factor]
        x = 1
        while x < length:
            dfx[x] = dfx[x-1]*1.01
            x += 1
        return dfx

def investment_ec (df, factor,length): #simulates investment in ecological capital
    dfx = pd.DataFrame()    
    if factor == "GDP":
        dfx[0] = df[factor]
        x = 1
        while x < length:
            dfx[x] = dfx[x-1]*1.01
            x += 1
        return dfx
    if factor == "Trade":
        dfx[0] = df[factor]
        x = 1
        while x < length:
            dfx[x] = dfx[x-1]*0.99
            x += 1
        return dfx
    if factor == "Unemp":
        dfx[0] = df[factor]
        x = 1
        while x < length:
            dfx[x] = dfx[x-1]*0.99
            x += 1
        return dfx
    if factor == "Resources":
        dfx[0] = df[factor]
        x = 1
        while x < length:
            dfx[x] = dfx[x-1]*0.99
            x += 1
        return dfx
    if factor == "IO":
        dfx[0] = df[factor]
        x = 1
        while x < length:
            dfx[x] = dfx[x-1]*0.99
            x += 1
        return dfx
    if factor == "EPI":
        dfx[0] = df[factor]
        x = 1
        while x < length:
            dfx[x] = dfx[x-1]*1.01
            x += 1
        return dfx

def investment_tc(df, factor,length): #simulates investment in technology
    dfx = pd.DataFrame()    
    if factor == "GDP":
        dfx[0] = df[factor]
        x = 1
        while x < length:
            dfx[x] = dfx[x-1]*1.01
            x += 1
        return dfx
    if factor == "Trade":
        dfx[0] = df[factor]
        x = 1
        while x < length:
            dfx[x] = dfx[x-1]*1.01
            x += 1
        return dfx
    if factor == "Unemp":
        dfx[0] = df[factor]
        x = 1
        while x < length:
            dfx[x] = dfx[x-1]*0.99
            x += 1
        return dfx
    if factor == "Resources":
        dfx[0] = df[factor]
        x = 1
        while x < length:
            dfx[x] = dfx[x-1]*1.01
            x += 1
        return dfx
    if factor == "IO":
        dfx[0] = df[factor]
        x = 1
        while x < length:
            dfx[x] = dfx[x-1]*1.01
            x += 1
        return dfx
    if factor == "EPI":
        dfx[0] = df[factor]
        x = 1
        while x < length:
            dfx[x] = dfx[x-1]*0.99
            x += 1
        return dfx

def scorer(df): #creates score of based on GDP per capita. 
    a = df.columns
    df["Score"]= df[a[0]]
    for i in rng:
        if df["GDPcap"].loc[i] < 5000:
            df["Score"].loc[i] = 0
        if 5000 < df["GDPcap"].loc[i] < 10000:
            df["Score"].loc[i] = 1
        if 10000 < df["GDPcap"].loc[i] < 15000:
            df["Score"].loc[i] = 2
        if 15000 < df["GDPcap"].loc[i] < 25000:
            df["Score"].loc[i] = 3
        if 25000 < df["GDPcap"].loc[i] < 30000:
            df["Score"].loc[i] = 4
        if 30000 < df["GDPcap"].loc[i] < 35000:
            df["Score"].loc[i] = 5
        if 35000 < df["GDPcap"].loc[i] < 42000:
            df["Score"].loc[i] = 6
        if 42000 < df["GDPcap"].loc[i] < 50000:
            df["Score"].loc[i] = 7
        if 50000 < df["GDPcap"].loc[i] < 55000:
            df["Score"].loc[i] = 8
        if 55000 < df["GDPcap"].loc[i] < 65000:
            df["Score"].loc[i] = 9
        if df["GDPcap"].loc[i] > 65000:
            df["Score"].loc[i] = 10
    return df

def change(df_old,df_new): #outputs the difference factors between two data frames.
    dfnew = pd.DataFrame()
    c1 = df_old.columns
    for i in c1:
        dfnew[i]=df_new[i]/df_old[i]
    return dfnew


def landchange(economyold, economynew, landuseold): #returns a dataframe with changed land use based on new economic attributes
    ln = pd.DataFrame()
    
    delta = change(economyold, economynew) #change between dataframes
    delta["IO"] = delta["IO"]*0.5
    delta.fillna(1, inplace = True)
                      
    ln["Urban"] = (landuseold["Urban"]+delta["IO"])/(delta["Resources"]+delta["EPI"])
    ln["Agriculture"] = (landuseold["Agriculture"]+delta["IO"]+delta["Resources"])/delta["EPI"]
    ln["Biotic"] = (landuseold["Biotic"] + delta["EPI"])/(delta["Resources"] + delta["IO"])
    ln["Abiotic"] = (landuseold["Abiotic"] + delta["EPI"])/(delta["Resources"]+delta["IO"])
    ln["Water"] = landuseold["Water"]
    ln = ln[["Abiotic", "Agriculture", "Biotic", "Urban", "Water"]]
    
    ln.replace([np.inf, -np.inf],0, inplace = True)
    ln.fillna(0,inplace = True)
    
    #resulting factors need to be multiplied by a factor to add to one again so it is usable as percentage
    lnnw = ln[["Abiotic", "Agriculture", "Biotic", "Urban"]]
    lnnw["Sum"] = lnnw["Abiotic"]*0
    for i in range(0,320):
        lnnw["Sum"].loc[i] = lnnw.loc[i].sum()    
    multfactor = pd.DataFrame()
    multfactor["M"]=ln["Water"]*0
    multfactor["M"] = (1-landuse["Water"])/lnnw["Sum"]
    multfactor.replace(np.inf, 1, inplace = True)
    multfactor.fillna(1,inplace = True)
   
    cc = ["Abiotic", "Agriculture", "Biotic", "Urban"]
    
    for i in range(320): 
        for c in cc:
            ln[c].loc[i] = float(ln[c].loc[i])*multfactor["M"].loc[i]
        
    return ln


def outputter(policy,df,landuse): #combines all of the above and outputs new economic data and new land use data
    wdata = pd.DataFrame()
    gdp = pd.DataFrame()
    trade = pd.DataFrame()   
    unemp = pd.DataFrame()
    resources = pd.DataFrame()   
    io = pd.DataFrame()   
    pop = pd.DataFrame()
    gdpcap = pd.DataFrame()
        
    if policy == "invest in tech":
        policy = investment_tc
        pop[0] = df["Population"]
        for i in range(1,10):
            pop[i] = pop[0]
              
        gdp = policy(df,"GDP",10) #parameter dataframes
        trade = policy(df,"Trade",10)
        unemp = policy(df,"Unemp",10)
        io = policy(df,"IO",10)
        epi = policy(df,"EPI",10)
        resources = policy(df,"Resources",10)

        gdpcap[0] = gdp[0]/pop[0]*1000000    
        gdpcap.fillna(0, inplace=True)
        for i in range(1,10):
            gdpcap[i] = gdpcap[0]       
            
        x = 1
        popgain = migration          
        while x < 10: #CELLULAR AUTOMATA for MIGRATION - migration depends on differences of GDP per capita
            for i in rng: #iterates through all triangles
                for e in prox[i]: #iterates through neighbours
                    if pop[x][i] > 10000 and pop[x][e] > 10000 and abs((gdpcap[x][e]-gdpcap[x][i])*1) > 10000:
                        if gdpcap[x][e] > gdpcap[x][i]:
                            pop[x][i] = pop[x-1][i]-abs((gdpcap[x][e]-gdpcap[x][i])*0.5)
                            pop[x][e] = pop[x-1][e]+abs((gdpcap[x][e]-gdpcap[x][i])*0.5)
                            gdpcap[x][i] = gdp[x-1][i]/pop[x-1][i]*1000000
                        if gdpcap[x][e] < gdpcap[x][i]:
                            pop[x][i] = pop[x-1][i]+abs((gdpcap[x][e]-gdpcap[x][i])*0.5) 
                            pop[x][e] = pop[x-1][e]-abs((gdpcap[x][e]-gdpcap[x][i])*0.5)
                            gdpcap[x][i] = gdp[x-1][i]/pop[x-1][i]*1000000
            x += 1
               
        wdata["GDP"] = gdp[9]
        wdata["Trade"] = trade[9]
        wdata["Unemp"] = unemp[9]
        wdata["IO"] = io[9]
        wdata["EPI"] = epi[9]
        wdata["Population"] = pop[9]
        wdata["GDPcap"] = gdpcap[9]
        wdata["Resources"] = resources[9]
        wdata = scorer(wdata) #new dataframe with all economic attributes
            
    if policy == "invest in ecology":
        policy = investment_ec
        pop[0] = df["Population"]
        for i in range(1,10):
            pop[i] = pop[0]
              
        gdp = policy(df,"GDP",10) #parameter dataframes
        trade = policy(df,"Trade",10)
        unemp = policy(df,"Unemp",10)
        io = policy(df,"IO",10)
        epi = policy(df,"EPI",10)
        resources = policy(df,"Resources",10)

        gdpcap[0] = gdp[0]/pop[0]*1000000    
        gdpcap.fillna(0, inplace=True)
        for i in range(1,10):
            gdpcap[i] = gdpcap[0]       
            
        x = 1
        popgain = migration          
        while x < 10: #CELLULAR AUTOMATA for MIGRATION
            for i in rng: #iterates through all triangles
                for e in prox[i]: #iterates through neighbours
                    if pop[x][i] > 10000 and pop[x][e] > 10000 and abs((gdpcap[x][e]-gdpcap[x][i])*1) > 10000:#iterates through neighbours
                        if gdpcap[x][e] > gdpcap[x][i]:
                            pop[x][i] = pop[x-1][i]-abs((gdpcap[x][e]-gdpcap[x][i])*0.5) #simulates migration
                            pop[x][e] = pop[x-1][e]+abs((gdpcap[x][e]-gdpcap[x][i])*0.5)
                            gdpcap[x][i] = gdp[x-1][i]/pop[x-1][i]*1000000
                        if gdpcap[x][e] < gdpcap[x][i]:
                            pop[x][i] = pop[x-1][i]+abs((gdpcap[x][e]-gdpcap[x][i])*0.5) #simulates migration
                            pop[x][e] = pop[x-1][e]-abs((gdpcap[x][e]-gdpcap[x][i])*0.5)
                            gdpcap[x][i] = gdp[x-1][i]/pop[x-1][i]*1000000
            x += 1
               
        wdata["GDP"] = gdp[9]
        wdata["Trade"] = trade[9]
        wdata["Unemp"] = unemp[9]
        wdata["IO"] = io[9]
        wdata["EPI"] = epi[9]
        wdata["Population"] = pop[9]
        wdata["GDPcap"] = gdpcap[9]
        wdata["Resources"] = resources[9]
        wdata = scorer(wdata)

    if policy == "invest in society":
        policy = investment_hd
        pop[0] = df["Population"]
        for i in range(1,10):
            pop[i] = pop[0]
              
        gdp = policy(df,"GDP",10) #parameter dataframes
        trade = policy(df,"Trade",10)
        unemp = policy(df,"Unemp",10)
        io = policy(df,"IO",10)
        epi = policy(df,"EPI",10)
        resources = policy(df,"Resources",10)

        gdpcap[0] = gdp[0]/pop[0]*1000000    
        gdpcap.fillna(0, inplace=True)
        for i in range(1,10):
            gdpcap[i] = gdpcap[0]       
            
        x = 1
        popgain = migration          
        while x < 10: #CELLULAR AUTOMATA for MIGRATION
            for i in rng: #iterates through all triangles
                for e in prox[i]: #iterates through neighbours
                    if pop[x][i] > 10000 and pop[x][e] > 10000 and abs((gdpcap[x][e]-gdpcap[x][i])*1) > 10000:#iterates through neighbours
                        if gdpcap[x][e] > gdpcap[x][i]:
                            pop[x][i] = pop[x-1][i]-abs((gdpcap[x][e]-gdpcap[x][i])*0.5) #simulates migration
                            pop[x][e] = pop[x-1][e]+abs((gdpcap[x][e]-gdpcap[x][i])*0.5)
                            gdpcap[x][i] = gdp[x-1][i]/pop[x-1][i]*1000000
                        if gdpcap[x][e] < gdpcap[x][i]:
                            pop[x][i] = pop[x-1][i]+abs((gdpcap[x][e]-gdpcap[x][i])*0.5) #simulates migration
                            pop[x][e] = pop[x-1][e]-abs((gdpcap[x][e]-gdpcap[x][i])*0.5)
                            gdpcap[x][i] = gdp[x-1][i]/pop[x-1][i]*1000000
            x += 1
               
        wdata["GDP"] = gdp[9]
        wdata["Trade"] = trade[9]
        wdata["Unemp"] = unemp[9]
        wdata["IO"] = io[9]
        wdata["EPI"] = epi[9]
        wdata["Population"] = pop[9]
        wdata["GDPcap"] = gdpcap[9]
        wdata["Resources"] = resources[9]
        wdata = scorer(wdata)
            
        
    if policy == isolation:
        policy = sim_iso_fl

        pop[0] = economy["Population"]
        for i in range(1,10):
            pop[i] = pop[0]
               
        gdp = policy(df,"GDP") #parameter dataframes
        trade = policy(df,"Trade")
        unemp = policy(df,"Unemp")
        io = policy(df,"IO")
        epi = policy(df,"EPI")
        resources = policy(df,"Resources")

        gdpcap[0] = gdp[0]/pop[0]*1000000    
        gdpcap.fillna(0, inplace=True)
        for i in range(1,10):
            gdpcap[i] = gdpcap[0]  
            
        x = 1
        popgain = migration
        while x < 10: #CELLULAR AUTOMATA for MIGRATION
            for i in rng: #iterates through all triangles
                for e in prox[i]: #iterates through neighbours
                    if pop[x][i] > 10000 and pop[x][e] > 10000 and abs((gdpcap[x][e]-gdpcap[x][i])*1) > 10000:#iterates through neighbours
                        if gdpcap[x][e] > gdpcap[x][i]:                        
                            pop[x][i] = pop[x-1][i]-abs((gdpcap[x][e]-gdpcap[x][i])*0.5) #simulates migration
                            pop[x][e] = pop[x-1][e]+abs((gdpcap[x][e]-gdpcap[x][i])*0.5)
                            gdpcap[x][i] = gdp[x-1][i]/pop[x-1][i]*1000000
                        if gdpcap[x][e] < gdpcap[x][i]:
                            pop[x][i] = pop[x-1][i]+abs((gdpcap[x][e]-gdpcap[x][i])*0.5) #simulates migration
                            pop[x][e] = pop[x-1][e]-abs((gdpcap[x][e]-gdpcap[x][i])*0.5)
                            gdpcap[x][i] = gdp[x-1][i]/pop[x-1][i]*1000000
            x += 1
        
        
        wdata["GDP"] = gdp[9]
        wdata["Trade"] = trade[9]
        wdata["Unemp"] = unemp[9]
        wdata["IO"] = io[9]
        wdata["EPI"] = epi[9]
        wdata["Population"] = pop[9]
        wdata["GDPcap"] = gdpcap[9]
        wdata["Resources"] = resources[9]
        
        wdata = scorer(wdata)
     
    if policy == ai:
        policy = sim_ai_fl
        pop[0] = economy["Population"]
        for i in range(1,10):
            pop[i] = pop[0]
               
        gdp = policy(df,"GDP") #parameter dataframes
        trade = policy(df,"Trade")
        unemp = policy(df,"Unemp")
        io = policy(df,"IO")
        epi = policy(df,"EPI")
        resources = policy(df,"Resources")
        
        gdpcap[0] = gdp[0]/pop[0]*1000000    
        gdpcap.fillna(0, inplace=True)
        for i in range(1,10):
            gdpcap[i] = gdpcap[0]  
            
        x = 1
        popgain = migration
        while x < 10: #CELLULAR AUTOMATA for MIGRATION
            for i in rng: #iterates through all triangles
                for e in prox[i]:
                    if pop[x][i] > 10000 and pop[x][e] > 10000 and abs((gdpcap[x][e]-gdpcap[x][i])*1) > 10000:#iterates through neighbours
                        if gdpcap[x][e] > gdpcap[x][i]:
                            pop[x][i] = pop[x-1][i]-abs((gdpcap[x][e]-gdpcap[x][i])*0.5) #simulates migration
                            pop[x][e] = pop[x-1][e]+abs((gdpcap[x][e]-gdpcap[x][i])*0.5)
                            gdpcap[x][i] = gdp[x-1][i]/pop[x-1][i]*1000000
                        if gdpcap[x][e] < gdpcap[x][i]:
                            pop[x][i] = pop[x-1][i]+abs((gdpcap[x][e]-gdpcap[x][i])*0.5) #simulates migration
                            pop[x][e] = pop[x-1][e]-abs((gdpcap[x][e]-gdpcap[x][i])*0.5)
                            gdpcap[x][i] = gdp[x-1][i]/pop[x-1][i]*1000000
            x += 1
        
        
        wdata["GDP"] = gdp[9]
        wdata["Trade"] = trade[9]
        wdata["Unemp"] = unemp[9]
        wdata["IO"] = io[9]
        wdata["EPI"] = epi[9]
        wdata["Population"] = pop[9]
        wdata["GDPcap"] = gdpcap[9]
        wdata["Resources"] = resources[9]
        wdata = scorer(wdata)
   
    abiotic = pd.DataFrame()
    agriculture = pd.DataFrame()
    biotic = pd.DataFrame()
    urban = pd.DataFrame()
    water = pd.DataFrame()
    
    landuselist = [abiotic, agriculture, biotic, urban,water]
    landusestring = ["Abiotic", "Agriculture", "Biotic", "Urban"]
    landusenew = landchange(df, wdata, landuse)
    water[0] = landusenew["Water"]
    
#    for i in landuselist:
#        for s in landusestring:                
#            i[0] = landuse[s]
#            for e in range(1,9):
#                i[e]=i[0]*np.nan
#            i[9]=landusenew[s]
    abiotic[0] = landuse["Abiotic"]
    for i in range(1,9):
        abiotic[i] = abiotic[0]*np.nan
    abiotic[9] = landusenew["Abiotic"]
    abiotic.interpolate(method='linear', axis=1, limit=None, inplace=True)
    
    agriculture[0] = landuse["Agriculture"]
    for i in range(1,9):
        agriculture[i] = agriculture[0]*np.nan
    agriculture[9] = landusenew["Agriculture"]
    agriculture.interpolate(method='linear', axis=1, limit=None, inplace=True)
    
    urban[0] = landuse["Urban"]
    for i in range(1,9):
        urban[i] = urban[0]*np.nan
    urban[9] = landusenew["Urban"]
    urban.interpolate(method='linear', axis=1, limit=None, inplace=True)
    
    biotic[0] = landuse["Biotic"]
    for i in range(1,9):
        biotic[i] = biotic[0]*np.nan
    biotic[9] = landusenew["Biotic"]
    biotic.interpolate(method='linear', axis=1, limit=None, inplace=True)
    
       
    for i in range(1,10):
        water[i] = water[0]
    
    #wdata is the new base economic dataframe for next turn
    return wdata, gdp, trade, unemp, io, epi, gdpcap, pop, resources,landusenew,abiotic, agriculture,biotic,urban,water



############################### EXPORT FOR DATA  VISUALIZATION IN HOUDINI  ###########################################
############################# RUN ONLY ONCE FOR EXPORT PURPOSES / KEEP HASHED  ###########################################

##empty dataframes
#wdata=pd.DataFrame()
#landuse = landuse
#
#gdp = pd.DataFrame()
#trade = pd.DataFrame()
#resources = pd.DataFrame()
#unemp = pd.DataFrame()
#io = pd.DataFrame()
#epi = pd.DataFrame()
#gdpcap = pd.DataFrame()
#pop = pd.DataFrame()
#score = pd.DataFrame()
#
#abiotic = pd.DataFrame()
#agriculture = pd.DataFrame()
#biotic = pd.DataFrame()
#urban = pd.DataFrame()
#water = pd.DataFrame()
#
##policies or scenarios to test in order
#
#firstpolicy = "invest in tech"
#
#policylist=[ "ai", "isolation", "invest in society", "invest in ecology" ]
#
##dataset generation
#
#policy = firstpolicy
#gdp = outputter(policy, economy,landuse)[1]
#trade = outputter(policy, economy,landuse)[2]
#unemp = outputter(policy, economy,landuse)[3]
#io = outputter(policy, economy,landuse)[4]
#epi = outputter(policy, economy,landuse)[5]
#gdpcap = outputter(policy, economy,landuse)[6]
#pop = outputter(policy, economy,landuse)[7]
#resources = outputter(policy, economy,landuse)[8]
#abiotic = outputter(policy, economy,landuse)[10]
#agriculture = outputter(policy, economy,landuse)[11]
#biotic = outputter(policy, economy,landuse)[12]
#urban = outputter(policy, economy,landuse)[13]
#water = outputter(policy, economy,landuse)[14]
#landuse = outputter(policy, economy,landuse)[9]
#wdata = outputter(policy, economy,landuse)[0]
#
#for i in policylist: #iterates through given policies
#    policy = i
#    gdp = pd.concat([gdp,outputter(policy, wdata,landuse)[1]],axis = 1)
#    trade = pd.concat([trade,outputter(policy, wdata,landuse)[2]],axis = 1)
#    unemp = pd.concat([unemp,outputter(policy, wdata,landuse)[3]],axis = 1)
#    io = pd.concat([io,outputter(policy, wdata,landuse)[4]],axis = 1)
#    epi = pd.concat([epi,outputter(policy, wdata,landuse)[5]],axis = 1)
#    gdpcap = pd.concat([gdpcap,outputter(policy, wdata,landuse)[6]],axis = 1)
#    pop = pd.concat([pop,outputter(policy, wdata,landuse)[7]],axis = 1)
#    resources = pd.concat([resources,outputter(policy,wdata,landuse)[8]],axis = 1)
#    abiotic = pd.concat([abiotic,outputter(policy, wdata,landuse)[10]],axis = 1)
#    agriculture = pd.concat([agriculture,outputter(policy,wdata,landuse)[11]],axis = 1)
#    biotic = pd.concat([biotic,outputter(policy, wdata,landuse)[12]],axis = 1)
#    urban = pd.concat([urban,outputter(policy, wdata,landuse)[13]],axis = 1)
#    landuse = outputter(policy, wdata,landuse)[9]
#    wdata = outputter(policy, wdata,landuse)[0]
#
##renames columns 
#
#gdp.columns = np.arange(len(gdp.columns-1))
#trade.columns = np.arange(len(trade.columns-1))
#unemp.columns = np.arange(len(unemp.columns-1))
#io.columns = np.arange(len(io.columns-1))
#epi.columns = np.arange(len(epi.columns-1))
#gdpcap.columns = np.arange(len(gdpcap.columns-1))
#pop.columns = np.arange(len(pop.columns-1))
#resources.columns = np.arange(len(resources.columns-1))
#abiotic.columns = np.arange(len(pop.columns-1))
#agriculture.columns = np.arange(len(agriculture.columns-1))
#biotic.columns = np.arange(len(biotic.columns-1))
#urban.columns = np.arange(len(urban.columns-1))
#
##crites csvs
#wdata.to_csv("wdata.csv")
#gdp.to_csv("gdp.csv")
#trade.to_csv("trade.csv")
#unemp.to_csv("unemp.csv")
#io.to_csv("io.csv")
#epi.to_csv("epi.csv")
#gdpcap.to_csv("gdpcap.csv")
#pop.to_csv("pop.csv")
#resources.to_csv("resources.csv")
#score.to_csv("score.csv")
#abiotic.to_csv("abiotic.csv")
#agriculture.to_csv("agriculture.csv")
#biotic.to_csv("biotic.csv")
#urban.to_csv("urban.csv")


############################## GAME###########################################
#wdata = scorer(economy)
#
#gdpcap = pd.DataFrame()
#
#
#print ("\n")
#print ("\n")
#print ("\n")
#print ("Welcome to the Planet Maker")
#time.sleep(1)
#name = raw_input ("Please enter your name:  ")
#print ("\n")
#print 'Hello '+name,'! You will now simulate the world under specific conditions.'
#time.sleep(3)
#print ("If the score of a country goes below 3, then they are below the poverty line. If it is above 7, they are doing very well")
#print "\n"
#time.sleep(3)
#print ("For evaluation matters, you only see the economic performace of the Greater New York Area, Coastal West Africa, and South East Asia")
#print "\n"
#time.sleep(3)
#print ("This is the world right now:")
#time.sleep(2)
#print ("\n")
#print "New York Area: "
#print wdata.loc[3]
#print ("\n")
#print "Ghana: "
#print wdata.loc[37]
#print ("\n")
#print "Vietnam:"
#print wdata.loc[107]
#
#print ("\n")
#time.sleep(2)
#print ("Every ten years, you can choose different policies: isolationism or artificial intelligence, or investment in tech, society or ecology")
#time.sleep(1)
#print ("\n")
#
#
#turn = 1
#gamelength = 3
#while turn <= gamelength:
#    print ("Please enter the policies exactly like this: isolation, ai, invest in tech, invest in society, invest in ecology")
#    print ("\n")
#    pol = raw_input("It's year 2028. Policy choice for the next 10 years: ")
#    gdpcap = pd.concat([gdpcap,outputter(pol,wdata,landuse)[6]],axis = 1)
#    wdata = outputter(pol,wdata,landuse)[0]    
#    print "New York Area: "
#    print wdata.loc[3]
#    print ("\n")
#    print "Coastal West Africa: "
#    print wdata.loc[37]
#    print ("\n")
#    print "South East Asia:"
#    print wdata.loc[107]
#    print ("\n")
#    time.sleep(2)
#    turn +=1
#
#
#print "This is how well you have done:"
#
#gdpcap_nyc = list(gdpcap.loc[3])
#gdpcap_ghana = list(gdpcap.loc[37])
#gdpcap_vietnam = list(gdpcap.loc[107])
#plt.plot(gdpcap_nyc)
#plt.plot(gdpcap_ghana)
#plt.plot(gdpcap_vietnam)
#plt.legend(["Greater New York Area", "Coastal West Africa", "South East Asia"])
#plt.show()
