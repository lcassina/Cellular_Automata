"""Provides a scripting component.
    Inputs:
        x: The x script variable
        y: The y script variable
    Output:
        a: The a output variable"""

__author__ = "Shervin Azadi"

import rhinoscriptsyntax as rs
import Rhino.Geometry as rg
import clr

def SubdivideTriangleMesh(M, SubdivisionCount):

    #    if(M.Faces.TriangleCount != M.Faces.Count){throw new Exception("the input mesh must be triangular!");}
    #    if(SubdivisionCount < 1){throw new Exception("subdivision count cannot be smaller than 1");}
    #    if(SubdivisionCount > 4){throw new Exception("oh oh! I'm not sure this is a good idea! hack the code if you really want to do this");}
    
    for iC in range(SubdivisionCount):

        FC = M.Faces.Count
        SubMesh = rg.Mesh()
        for k in range(FC):
            v0f = clr.StrongBox[rg.Point3f]()
            v1f = clr.StrongBox[rg.Point3f]()
            v2f = clr.StrongBox[rg.Point3f]()
            v3f = clr.StrongBox[rg.Point3f]()

            M.Faces.GetFaceVertices(k, v0f, v1f, v2f, v3f)
            v0 = rg.Point3d(v0f.Value)
            v1 = rg.Point3d(v1f.Value)
            v2 = rg.Point3d(v2f.Value)

            v01 = 0.5 * (v0 + v1)
            v12 = 0.5 * (v1 + v2)
            v20 = 0.5 * (v2 + v0)

            t0 = rg.Mesh()
            t1 = rg.Mesh()
            t2 = rg.Mesh()
            t3 = rg.Mesh()

            vt0 = [v0,v01,v20]
            vt1 = [v1,v12,v01]
            vt2 = [v2,v20,v12]
            vt3 = [v01,v12,v20]
            for i in range(3):
                t0.Vertices.Add(vt0[i])
                t1.Vertices.Add(vt1[i])
                t2.Vertices.Add(vt2[i])
                t3.Vertices.Add(vt3[i])

            t0.Faces.AddFace(0, 1, 2)
            t1.Faces.AddFace(0, 1, 2)
            t2.Faces.AddFace(0, 1, 2)
            t3.Faces.AddFace(0, 1, 2)

            SubMesh.Append(t0)
            SubMesh.Append(t1)
            SubMesh.Append(t2)
            SubMesh.Append(t3)


        M = SubMesh

    return M

A = SubdivideTriangleMesh(ToBeSubdivided, subC)

